import argparse
import os
from glob import glob
import pandas as pd

import gensim.downloader as api
import h5py
import os

from loguru import logger
from nltk import word_tokenize
import numpy as np
from tqdm import tqdm

MODELS = {
    'glove': 'glove-wiki-gigaword-300',
    'word2vec': 'word2vec-google-news-300',
    'fasttext': 'fasttext-wiki-news-subwords-300'
}

parser = argparse.ArgumentParser()
parser.add_argument('transcriptfile',
                    default='labels_processed/all_transcripts.csv',
                    type=str,
                    nargs="?",
                    help="All transcriptions (cleaned)")
parser.add_argument('--transcriptdir', type=str, default='labels_processed')
parser.add_argument('-w', type=int, default=4, help="Worker count")
parser.add_argument('--filterlen', default=0, type=int)
parser.add_argument('--filterby', type=str, default='Participant')
parser.add_argument('-m',
                    '--model',
                    default='word2vec',
                    choices=MODELS,
                    type=str)
parser.add_argument('-avg', '--average', action='store_true', default=False)
args = parser.parse_args()

logger.info("Passed args:")
for k, v in vars(args).items():
    logger.info("{}:{}".format(k, v))

logger.info(f"Loading pretrained model {args.model}")
model = api.load(MODELS[args.model])

subsetfiles = [
    'labels/train_split_Depression_AVEC2017.csv',
    'labels/dev_split_Depression_AVEC2017.csv'
]
averaged = "_sentavg" if args.average else ""
output_files = [
    f'hdf5/text/{args.model}_train_pretrained{averaged}.h5',
    f'hdf5/text/{args.model}_dev_pretrained{averaged}.h5',
]
for subsetfile, outputfile in zip(subsetfiles, output_files):
    # Extracting features for the Participant IDs
    subset_df = pd.read_csv(subsetfile)
    speakers = subset_df['Participant_ID'].values

    with h5py.File(outputfile, 'w') as store:
        for speaker in tqdm(speakers):
            # PRocess transcript first to get start_end
            transcript_file = glob(
                os.path.join(args.transcriptdir, str(speaker)) +
                '*TRANSCRIPT.csv')[0]
            transcript_df = pd.read_csv(transcript_file, sep='\t')
            # Filter for participant only
            if args.filterby:
                transcript_df = transcript_df[transcript_df.speaker ==
                                              args.filterby]
            transcript_df = transcript_df[
                transcript_df.value.str.split().apply(len) > args.filterlen]
            features = []
            for paragraph in transcript_df.value.str.lower().values:
                paragraph_feature = []
                for word in paragraph.split(' '):
                    if word in model:
                        vec = model[word.lower()]
                    else:
                        vec = np.zeros(300)
                    paragraph_feature.append(vec)
                paragraph_feature = np.array(paragraph_feature)
                if args.average:  # Sentence level average
                    paragraph_feature = paragraph_feature.mean(0)[None,:]
                features.extend(paragraph_feature)
            features = np.array(features)
            store[str(speaker)] = features
    logger.info(f"Dumping to file {outputfile}")
