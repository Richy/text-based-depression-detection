import argparse
import numpy as np
import os
import pandas as pd
from glob import glob
import h5py
from loguru import logger
from tqdm import tqdm
from gensim.models.fasttext import FastText, FastTextKeyedVectors
from gensim.models.word2vec import Word2Vec
from gensim.utils import tokenize

MODELS = ['fasttext', 'word2vec']

parser = argparse.ArgumentParser()
parser.add_argument('transcriptfile',
                    default='labels_processed/all_transcripts.csv',
                    type=str,
                    nargs="?",
                    help="All transcriptions (cleaned)")
parser.add_argument('--transcriptdir', type=str, default='labels_processed')
parser.add_argument('-w', type=int, default=4, help="Worker count")
parser.add_argument('--filterlen', default=0, type=int)
parser.add_argument('--filterby', type=str, default='Participant')
parser.add_argument('-avg', '--average', action='store_true', default=False)
parser.add_argument('--model', '-m', choices=MODELS, default='word2vec')
args = parser.parse_args()

# Pretraining of any model
df = pd.read_csv(args.transcriptfile, sep='\t')
# Filter length ( remove uh yeah um .... )
df = df[df.value.str.split().apply(len) > args.filterlen]
all_text = df.value.values

for k, v in vars(args).items():
    logger.info(f"{k} : {v}")
logger.info(f"Data size: {all_text.shape}")
if args.model.lower() == 'fasttext':
    model = FastText(all_text,
                     size=300,
                     sg=1,
                     hs=1,
                     workers=args.w,
                     iter=40,
                     seed=41)

elif args.model.lower() == 'word2vec':
    model = Word2Vec(all_text,
                     size=300,
                     window=3,
                     min_count=3,
                     workers=args.w,
                     iter=50,
                     sample=1e-4,
                     seed=41)

subsetfiles = [
    'labels/train_split_Depression_AVEC2017.csv',
    'labels/dev_split_Depression_AVEC2017.csv'
]
averaged = "_sentavg" if args.average else ""
output_files = [
    f"hdf5/text/train_{args.model}{averaged}.h5",
    f'hdf5/text/dev_{args.model}{averaged}.h5'
]
for subsetfile, outputfile in zip(subsetfiles, output_files):
    # Extracting features for the Participant IDs
    subset_df = pd.read_csv(subsetfile)
    speakers = subset_df['Participant_ID'].values
    logger.info(f"Processing {subsetfile} to {outputfile}")

    with h5py.File(outputfile, 'w') as store:
        for speaker in tqdm(speakers):
            # PRocess transcript first to get start_end
            transcript_file = glob(
                os.path.join(args.transcriptdir, str(speaker)) +
                '*TRANSCRIPT.csv')[0]
            transcript_df = pd.read_csv(transcript_file, sep='\t')
            # Filter for participant only
            if args.filterby:
                transcript_df = transcript_df[transcript_df.speaker ==
                                              args.filterby]
            transcript_df = transcript_df[
                transcript_df.value.str.split().apply(len) > args.filterlen]
            features = []
            for paragraph in transcript_df.value.values:
                paragraph_feature = []
                for word in tokenize(paragraph):
                    if word in model:
                        vec = model[word.lower()]
                    else:
                        vec = np.zeros(300)
                    paragraph_feature.append(vec)
                paragraph_feature = np.array(paragraph_feature)
                if args.average:  # Sentence level average
                    paragraph_feature = paragraph_feature.mean(0)[None, :]
                features.extend(paragraph_feature)
            features = np.array(features)
            store[str(speaker)] = features
