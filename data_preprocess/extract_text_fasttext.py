import argparse
import numpy as np
import os
import pandas as pd
from glob import glob
import h5py
from loguru import logger
from tqdm import tqdm
from gensim.models.fasttext import FastText, FastTextKeyedVectors
from nltk import word_tokenize

parser = argparse.ArgumentParser()
parser.add_argument('transcriptfile',
                    default='labels_processed/all_transcripts.csv',
                    type=str,
                    nargs="?",
                    help="All transcriptions (cleaned)")
parser.add_argument('--transcriptdir', type=str, default='labels_processed')
parser.add_argument('-w', type=int, default=4, help="Worker count")
parser.add_argument('--filterlen', default=0, type=int)
parser.add_argument('--filterby', type=str, default='Participant')
parser.add_argument('--pretrained', default=False, action='store_true')
args = parser.parse_args()

if not args.pretrained:
    # Pretraining of Doc2Vec model
    df = pd.read_csv(args.transcriptfile, sep='\t')
    # Filter length ( remove uh yeah um .... )
    df = df[df.value.str.split().apply(len) > args.filterlen]
    all_text = df.value.values

    model = FastText(all_text,
                     size=300,
                     window=3,
                     min_count=3,
                     workers=args.w,
                     iter=50,
                     sample=1e-4,
                     seed=41)

else:
    model = FastTextKeyedVectors.load(
        'pretrained/fasttext_wiki-2020-latest-en.wv')

pretrained_flag = "_pretrained" if args.pretrained else ""
subsetfiles = [
    'labels/train_split_Depression_AVEC2017.csv',
    'labels/dev_split_Depression_AVEC2017.csv'
]
output_files = [
    'hdf5/text/train_fasttext{}.h5'.format(pretrained_flag),
    'hdf5/text/dev_fasttext{}.h5'.format(pretrained_flag)
]
for subsetfile, outputfile in zip(subsetfiles, output_files):
    # Extracting features for the Participant IDs
    subset_df = pd.read_csv(subsetfile)
    speakers = subset_df['Participant_ID'].values
    logger.info(f"Processing {subsetfile} to {outputfile}")

    with h5py.File(outputfile, 'w') as store:
        for speaker in tqdm(speakers):
            # PRocess transcript first to get start_end
            transcript_file = glob(
                os.path.join(args.transcriptdir, str(speaker)) +
                '*TRANSCRIPT.csv')[0]
            transcript_df = pd.read_csv(transcript_file, sep='\t')
            # Filter for participant only
            if args.filterby:
                transcript_df = transcript_df[transcript_df.speaker ==
                                              args.filterby]
            transcript_df = transcript_df[
                transcript_df.value.str.split().apply(len) > args.filterlen]
            features = []
            for paragraph in transcript_df.value.values:
                for word in paragraph.split(' '):
                    if word in model:
                        vec = model[word.lower()]
                    else:
                        vec = np.zeros(300)
                    features.append(vec)
            features = np.array(features)
            store[str(speaker)] = features
