import argparse
import os
from glob import glob
import pandas as pd
from gensim.models.word2vec import Word2VecKeyedVectors
from gensim.models.fasttext import FastTextKeyedVectors
from gensim.utils import tokenize

from scipy.stats import kurtosis
from scipy.stats import skew
import h5py
import os

from loguru import logger
from nltk import word_tokenize
import numpy as np
from tqdm import tqdm


def higher_order_stats(f, func_list):
    return np.concatenate([func(f, axis=0) for func in func_list])[None, :]


MODELS = {
    'word2vec':
    Word2VecKeyedVectors.load(
        'pretrained/word2vec_enwiki-latest-pages-articles.xml.wv'),
    'fasttext':
    FastTextKeyedVectors.load(
        'pretrained/fasttext_enwiki-latest-pages-articles.xml.wv')
}

parser = argparse.ArgumentParser()
parser.add_argument('transcriptfile',
                    default='labels_processed/all_transcripts.csv',
                    type=str,
                    nargs="?",
                    help="All transcriptions (cleaned)")
parser.add_argument('--transcriptdir', type=str, default='labels_processed')
parser.add_argument('-w', type=int, default=4, help="Worker count")
parser.add_argument('--filterlen', default=0, type=int)
parser.add_argument('--filterby', type=str, default='Participant')
parser.add_argument('-m',
                    '--model',
                    default='word2vec',
                    choices=MODELS,
                    type=str)
parser.add_argument('-avg', '--average', action='store_true', default=False)
parser.add_argument('-stat',
                    action='store_true',
                    default=False,
                    help="Stat pooling for sentences")
args = parser.parse_args()

logger.info("Passed args:")
for k, v in vars(args).items():
    logger.info("{}:{}".format(k, v))

logger.info(f"Loading pretrained model {args.model}")
model = MODELS[args.model]

subsetfiles = [
    'labels/train_split_Depression_AVEC2017.csv',
    'labels/dev_split_Depression_AVEC2017.csv'
]
averaged = "_sentavg" if args.average else ""
statpool = "_stat" if args.stat else ""
output_files = [
    f'hdf5/text/{args.model}_wikipre_train_pretrained{averaged}{statpool}.h5',
    f'hdf5/text/{args.model}_wikipre_dev_pretrained{averaged}{statpool}.h5',
]
for subsetfile, outputfile in zip(subsetfiles, output_files):
    # Extracting features for the Participant IDs
    subset_df = pd.read_csv(subsetfile)
    speakers = subset_df['Participant_ID'].values

    with h5py.File(outputfile, 'w') as store:
        for speaker in tqdm(speakers):
            # PRocess transcript first to get start_end
            transcript_file = glob(
                os.path.join(args.transcriptdir, str(speaker)) +
                '*TRANSCRIPT.csv')[0]
            transcript_df = pd.read_csv(transcript_file, sep='\t')
            # Filter for participant only
            if args.filterby:
                transcript_df = transcript_df[transcript_df.speaker ==
                                              args.filterby]
            transcript_df = transcript_df[
                transcript_df.value.str.split().apply(len) > args.filterlen]
            features = []
            for paragraph in transcript_df.value.str.lower().values:
                paragraph_feature = []
                for word in tokenize(paragraph):
                    if word in model:
                        vec = model[word]
                    else:
                        vec = np.zeros(300)
                    paragraph_feature.append(vec)
                paragraph_feature = np.array(paragraph_feature)
                if args.stat:
                    paragraph_feature = higher_order_stats(
                        paragraph_feature, (np.mean, np.median, np.min, np.max,
                                            np.std, skew, kurtosis))
                elif args.average:  # Sentence level average
                    paragraph_feature = paragraph_feature.mean(0)[None, :]
                features.extend(paragraph_feature)
            features = np.array(features)
            store[str(speaker)] = features
    logger.info(f"Dumping to file {outputfile}")
