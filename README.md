# Text based depression analysis


# Prequisites

The required python packages can be found in `requirements.txt`.

```
allennlp==0.9.0
pandas==0.24.2
librosa==0.7.0
tabulate==0.8.3
gensim==3.8.1
bert_serving_server==1.9.6
nltk==3.4.5
numpy==1.16.4
torch==1.2.0
tqdm==4.32.2
loguru==0.4.0
scipy==1.3.0
pytorch_ignite==0.2.0
fire==0.1.3
kaldi_io==0.9.1
imbalanced_learn==0.5.0
h5py==2.9.0
bert_serving==0.0.1
ignite==1.1.0
imblearn==0.0
scikit_learn==0.23.1
PyYAML==5.3.1
```

## Dataset 

The dataset is freely available for research purposes and can be found [here](http://dcapswoz.ict.usc.edu/).


## Preprocessing

The preprocessing feature extraction scripts can be found in `data_preprocess/`.

Pretraining of our fastText and word2vec embeddings can be found in `pretraining_wikipedia/`.

Word2Vec features require manual training. For this purpose, one should create some `all_transcripts.csv` file with the same format as all other `*TRANSCRIPTS.csv` files.

The easies way to do that is just use bash or pandas (python) to merge all files into one, removing all headers except the first one e.g.,

```bash
cd labels_processed;
python -c "from glob import glob as g; import pandas as pd; pd.concat([pd.read_csv(f,sep='\t') for f in g('*TRANS*.csv')]).to_csv('all_transcripts.csv', index=False, sep='\t')"
```

Then just run the `extract_text_gensim.py` script, which extracts `100dim` features into the `features/text` folder.

ELMo features can be rather directly downloaded and pretrained given one installed `allennlp`. 
However, BERT embeddings require the download of a [pretrained BERT](https://github.com/google-research/bert).
In order to have easy access to BERT embeddings we use [bert serving client](https://pypi.org/project/bert-serving-client/). Here we prepared a simple script `start_bert_server.sh` in order to start the serving client. We recommend the use of screen or tmux.

In order to extract the features, prepare a folder named `labels` and softlink the `train_split_Depression_AVEC2017.csv` and dev files into it.

Further prepare the transcripts for feature extraction by directly copying all transcripts into a dir called `labels_processed`.
In other cases pass `--transcriptdir OUTDIRNAME` to any of the extraction scripts.


# Running the code

The main script of this repo is `run.py`.

The code is centered around the config files placed at `config/`. Each parameter in these files can be modified for each run using google-fire e.g., if one opts to run a different model, just pass `--model GRU`. 

`run.py` the following options ( ran as `python run.py OPTION`):
* `train`: Trains a model given a config file (default is `config/text_lstm_deep.yaml`).
* `evaluate`: Evaluates a given trained model directory. Just pass the result of `train` to it.
* `evaluate_kfold`: Same as evaluate but runs multiple passes with kfold stratification.

## Notes

Due to the limited amount of training data, reproducing the results might be problematic. In our work we specifically pointed out that the proposed results are chosen as the peak over multiple runs. 

