# -*- coding: utf-8 -*-
# @Author: richman
# @Date:   2018-01-18 10:28:31
# @Last Modified by:   richman
# @Last Modified time: 2018-04-11
import kaldi_io
import numpy as np
import torch
import h5py
import pandas as pd
import random
from torch.utils import data as torch_data
import itertools
from imblearn.over_sampling import RandomOverSampler


class HDF5Dataset(torch_data.Dataset):
    """
    HDF5 dataset indexed by a labels dataframe. 
    Indexing is done via the dataframe since we want to preserve some storage
    in cases where oversampling is needed ( pretty likely )
    """
    def __init__(self,
                 h5file: h5py.File,
                 labels: pd.DataFrame,
                 transform=None,
                 colname=('Participant_ID', 'PHQ8_Score', 'PHQ8_Binary')):
        super().__init__()
        self._h5file = h5file
        self.dataset = None
        self._labels = labels
        self._colname = colname
        self._len = len(self._labels)
        # IF none is passed still use no transform at all
        self._transform = transform
        with h5py.File(self._h5file, 'r') as store:
            fname = self._labels.iloc[0].reindex(self._colname)[0]
            self.datadim = store[str(fname)].shape[-1]

    def __len__(self):
        return self._len

    def __getitem__(self, index):
        if self.dataset is None:
            self.dataset = h5py.File(self._h5file, 'r')
        fname, target_score, target_bin = self._labels.iloc[index].reindex(
            self._colname)
        data = self.dataset[str(fname)][()]
        data = torch.as_tensor(data).float()
        length = len(data)
        if self._transform:
            data = self._transform(data)
        return data, [target_score, target_bin], fname, length


def pad(tensorlist, batch_first=True, padding_value=0., min_length=None):
    # In case we have 3d tensor in each element, squeeze the first dim (usually 1)
    if len(tensorlist[0].shape) == 3:
        tensorlist = [ten.squeeze() for ten in tensorlist]
    padded_seq = torch.nn.utils.rnn.pad_sequence(tensorlist,
                                                 batch_first=batch_first,
                                                 padding_value=padding_value)
    if min_length and padded_seq.shape[
            1] < min_length:  # Pad to a minimum length
        new_pad = torch.zeros(size=(padded_seq.shape[0], min_length,
                                    padded_seq.shape[2]))
        new_pad[:, :padded_seq.shape[1], :] = padded_seq

    return padded_seq


def sequential_collate(batches):
    # sort length wise
    batches.sort(key=lambda x: len(x), reverse=True)
    seqs = []
    for data_seq in zip(*batches):
        if isinstance(data_seq[0],
                      (torch.Tensor, np.ndarray)):  # is tensor, then pad
            data_seq = pad(data_seq)
        elif type(data_seq[0]) is list or type(
                data_seq[0]) is tuple:  # is label or something, do not pad
            data_seq = torch.as_tensor(data_seq)
        elif type(data_seq[0]) is int:
            data_seq = torch.tensor(data_seq)
        seqs.append(data_seq)
    return seqs


class MinimumOccupancySampler(torch_data.Sampler):
    """
        docstring for MinimumOccupancySampler
        samples at least one instance from each class sequentially
    """
    def __init__(self, labels, sampling_mode='over', random_state=None):
        data_samples = labels.shape
        n_labels = len(np.unique(labels))
        label_to_idx_list, label_to_length = [], []
        self.random_state = np.random.RandomState(seed=random_state)
        for lb_idx in range(n_labels):
            label_indexes = np.where(labels == lb_idx)[0]
            self.random_state.shuffle(label_indexes)
            label_to_length.append(len(label_indexes))
            label_to_idx_list.append(label_indexes)

        self.longest_seq = max(label_to_length)
        self.data_source = np.empty((self.longest_seq, len(label_to_length)), dtype=int)
        # Each column represents one "single instance per class" data piece
        for ix, leng in enumerate(label_to_length):
            self.data_source[:leng, ix] = label_to_idx_list[ix]
        self.label_to_length = label_to_length
        self.label_to_idx_list = label_to_idx_list

        if sampling_mode == 'same':
            self.data_length = data_samples
        elif sampling_mode == 'over':  # Sample all items
            self.data_length = np.prod(self.data_source.shape)

    def _resample(self):
        for ix, leng in enumerate(self.label_to_length):
            leftover = self.longest_seq - leng
            random_idxs = np.random.randint(leng, size=leftover)
            self.data_source[leng:, ix] = self.label_to_idx_list[ix][random_idxs]


    def __iter__(self):
        self._resample()
        n_samples = len(self.data_source)
        random_indices = self.random_state.permutation(n_samples)
        data = np.concatenate(
            self.data_source[random_indices])[:self.data_length]
        return iter(data)

    def __len__(self):
        return self.data_length


def create_dataloader(
        data_hdf5,
        label_df,
        transform=None,
        batch_size: int = 1,
        num_workers: int = 2,
        shuffle: bool = True,
        sampler=None,
):
    """create_dataloader

    """
    # Shuffling means that this is training dataset, so oversample
    # if shuffle:
    # sampler = RandomOverSampler()
    # # Assume that label is Score, Binary, we take the binary to oversample
    # sample_index = 1 if len(labels[0]) == 2 else 0
    # # Dummy X data, y is the binary label
    # _, _ = sampler.fit_resample(torch.ones(len(features), 1),
    # [l[sample_index] for l in labels])
    # # Get the indices for the sampled data
    # indicies = sampler.sample_indices_
    # # reindex, new data is oversampled in the minority class
    # features, labels = [features[id] for id in indicies
    # ], [labels[id] for id in indicies]
    dataset = HDF5Dataset(data_hdf5, label_df)
    # Configure weights to reduce number of unseen utterances
    return torch_data.DataLoader(dataset,
                                 batch_size=batch_size,
                                 num_workers=num_workers,
                                 collate_fn=sequential_collate,
                                 shuffle=shuffle,
                                 sampler=sampler)


if __name__ == "__main__":
    from tqdm import tqdm
    lab_df = pd.read_csv('labels/train_split_Depression_AVEC2017.csv')
    min = MinimumOccupancySampler(np.stack(lab_df['PHQ8_Binary']))
    for a in tqdm(min):
        pass
