import torch
import math
import torch.nn as nn
# from TCN.tcn import TemporalConvNet


class AttentionPool(nn.Module):
    """Docstring for Attention. """
    def __init__(self, inputdim, outputdim=1, **kwargs):
        """TODO: to be defined1.

        :inputdim: TODO

        """
        nn.Module.__init__(self)
        self.attn = nn.Linear(inputdim, outputdim, bias=False)
        nn.init.normal_(self.attn.weight, std=0.05)

    def forward(self, x, length):
        batchsize = x.shape[0]
        timesteps = x.shape[1]
        idxs = torch.arange(timesteps, device='cpu').repeat(batchsize).view(
            batchsize, timesteps)
        mask = (idxs < length.view(-1, 1)).to(x.device)  # In case of GPU
        logits = self.attn(x)
        logits = (logits - logits.max()).exp()
        masked_logits = logits * mask.unsqueeze(-1)
        att_sum = masked_logits.sum(1, keepdim=True)
        weights = masked_logits / att_sum
        out = (weights * x).sum(dim=1)
        return {'output': out, 'weight': weights}


class PowerMeanPool(nn.Module):
    """Docstring for PowerMeanPooling. """
    def __init__(self, batchfirst=True, p=2, **kwargs):
        """TODO: to be defined.

        :Docstring for PowerMeanPooling.:

        """
        super().__init__()
        self.batchdim = 0 if batchfirst else 1
        self.timedim = 1 if batchfirst else 0
        self.p = p

    def forward(self, x, lengths):
        batchsize = x.shape[self.batchdim]
        timesize = x.shape[self.timedim]
        idxs = torch.arange(timesize, device='cpu').repeat(batchsize).view(
            batchsize, timesize)
        mask = (idxs < lengths.view(-1, 1)).to(x.device)
        x = x * mask.unsqueeze(-1)
        output = ((x**self.p).sum(self.timedim) /
                  lengths.unsqueeze(-1))**(1. / self.p)
        return {'output': output}


class MeanMaxPool(nn.Module):
    def __init__(self, dim=1, **kwargs):
        nn.Module.__init__(self)
        self._dim = dim
        self._maxpool1 = nn.MaxPool1d(2)
        self._avgpool1 = nn.AvgPool1d(4)
        self._avgpool2 = MeanPool()

    def forward(self, x, length):
        x = x.transpose(1, 2)
        x = self._avgpool1(x)
        x = self._maxpool1(x)
        x = x.transpose(1, 2)
        length = length / 8.
        return self._avgpool2(x, length)


class SelectPool(nn.Module):
    def __init__(self, select, dim=1, **kwargs):
        nn.Module.__init__(self)
        self._select = select
        self._dim = dim

    def forward(self, x, length):
        batchsize = x.shape[0]
        timesteps = x.shape[self._dim]
        idxs = torch.arange(timesteps).repeat(batchsize).view(
            batchsize, timesteps)
        mask = idxs < length.view(-1, 1)
        return {'output': x.select(self._dim, self._select)}


class MeanPool(nn.Module):
    def __init__(self, batch_first=True, **kwargs):
        nn.Module.__init__(self)
        self.batch_dim = 0 if batch_first else 1
        self.time_dim = 1 if batch_first else 0

    def forward(self, x, length):
        batchsize = x.shape[self.batch_dim]
        timesteps = x.shape[self.time_dim]
        idxs = torch.arange(timesteps, device='cpu').repeat(batchsize).view(
            batchsize, timesteps)
        mask = (idxs < length.view(-1, 1)).to(x.device)
        x = x * mask.unsqueeze(-1)
        return {
            'output':
            x.sum(self.time_dim) /
            mask.sum(self.time_dim).unsqueeze(self.time_dim)
        }


class StdPool(nn.Module):
    def __init__(self, batch_first=True, **kwargs):
        nn.Module.__init__(self)
        self.batch_dim = 0 if batch_first else 1
        self.time_dim = 1 if batch_first else 0
        self.meanpool = MeanPool(batch_first=batch_first)

    def forward(self, x, length):
        batchsize = x.shape[self.batch_dim]
        timesteps = x.shape[self.time_dim]
        idxs = torch.arange(timesteps, device='cpu').repeat(batchsize).view(
            batchsize, timesteps)
        mask = (idxs < length.view(-1, 1)).to(x.device)
        x = x * mask.unsqueeze(-1)
        mean = self.meanpool(x, length)['output']
        std = torch.sqrt(
            ((x - mean.unsqueeze(self.time_dim))**2).sum(self.time_dim) /
            mask.sum(self.time_dim).unsqueeze(self.time_dim))
        return {'output': std}


class FirstMaxPool(nn.Module):
    def __init__(self, batch_first=True, **kwargs):
        nn.Module.__init__(self)
        self.maxpool = MaxPool(batch_first)
        self.firstpool = SelectPool(0, dim=1)

    def forward(self, x, length):
        maxout = self.maxpool(x, length)
        firstout = self.firstpool(x, length)
        output = maxout['output'] + firstout['output']
        return {'output': output, 'weight': maxout['weight']}


class MaxPool(nn.Module):
    def __init__(self, batch_first=True, **kwargs):
        nn.Module.__init__(self)
        self.batch_dim = 0 if batch_first else 1
        self.time_dim = 1 if batch_first else 0

    def forward(self, x, length):
        batchsize = x.shape[self.batch_dim]
        timesteps = x.shape[self.time_dim]
        idxs = torch.arange(timesteps, device='cpu').repeat(batchsize).view(
            batchsize, timesteps)
        mask = (idxs < length.view(-1, 1)).to(x.device)
        x[~mask] = float('-inf')
        output, index = x.max(self.time_dim)
        return {'output': output, 'weight': index}


class StatPool(nn.Module):
    def __init__(self, batch_first=True, **kwargs):
        nn.Module.__init__(self)
        self.batch_dim = 0 if batch_first else 1
        self.time_dim = 1 if batch_first else 0
        self.meanpool = MeanPool(batch_first=batch_first)
        self.maxpool = MaxPool(batch_first=batch_first)

    def forward(self, x, length):
        batchsize = x.shape[self.batch_dim]
        timesteps = x.shape[self.time_dim]
        idxs = torch.arange(timesteps, device='cpu').repeat(batchsize).view(
            batchsize, timesteps)
        mask = (idxs < length.view(-1, 1)).to(x.device)
        x = x * mask.unsqueeze(-1)
        mean = self.meanpool(x, length)['output']
        std = torch.sqrt(
            ((x - mean.unsqueeze(self.time_dim))**2).sum(self.time_dim) /
            mask.sum(self.time_dim).unsqueeze(self.time_dim))
        maxval = self.maxpool(x, length)['output']
        return {'output': torch.cat((mean, std, maxval), -1)}


def parse_poolingfunction(poolingfunction_name='mean', **kwargs):
    if poolingfunction_name == 'mean':
        return MeanPool(**kwargs)
    if poolingfunction_name == 'mm':
        return MeanMaxPool(**kwargs)
    if poolingfunction_name == 'std':
        return StdPool(**kwargs)
    if poolingfunction_name == 'stat':
        return StatPool(**kwargs)
    if poolingfunction_name == 'max':
        return MaxPool(**kwargs)
    elif poolingfunction_name == 'attention':
        return AttentionPool(**kwargs)
    elif poolingfunction_name == 'first':
        return SelectPool(select=0, dim=1)
    elif poolingfunction_name == 'firstmax':
        return FirstMaxPool(select=0, dim=1)
    elif poolingfunction_name == 'last':
        return SelectPool(select=-1, dim=1)
    elif poolingfunction_name == 'powmean':
        return PowerMeanPool(**kwargs)
    else:
        raise ValueError(
            "Pooling function {} not available".format(poolingfunction_name))


def init_rnn(rnn):
    """init_rnn
    Initialized RNN weights, independent of type GRU/LSTM/RNN

    :param rnn: the rnn model 
    """
    for name, param in rnn.named_parameters():
        if 'bias' in name:
            nn.init.constant_(param, 0.0)
        elif 'weight' in name:
            nn.init.xavier_uniform_(param)


class GRU(torch.nn.Module):
    """GRUAttn class for Depression detection"""
    def __init__(self, inputdim: int, output_size: int, **kwargs):
        """

        :inputdim:int: Input dimension
        :output_size:int: Output dimension of GRUAttn
        :**kwargs: Other args, passed down to nn.GRUAttn


        """
        torch.nn.Module.__init__(self)
        model_kwargs = {
            'hidden_size': kwargs.get('hidden_size', 128),
            'num_layers': kwargs.get('num_layers', 3),
            'dropout': kwargs.get('dropout', 0.2),
            'bidirectional': kwargs.get('bidirectional', True)
        }
        self.net = nn.GRU(input_size=inputdim, **model_kwargs)
        init_rnn(self.net)
        pooling_kwargs = {
            'pooling': kwargs.get('pooling', 'attention'),
            'outputdim': kwargs.get(
                'outputdim',
                1),  # In case of attention: 2 or 1 or how many attentions
        }
        self.pooling = parse_poolingfunction(
            pooling_kwargs['pooling'],
            inputdim=model_kwargs['hidden_size'] * 2,
            outputdim=pooling_kwargs['outputdim'])
        linear_dim = model_kwargs['hidden_size'] * 2
        if pooling_kwargs['pooling'] == 'stat':
            linear_dim *= 3
        self.outputlayer = nn.Linear(linear_dim, output_size)

    def forward(self, x, lengths):
        """Forwards input vector through network

        :x: input tensor of shape (B, T, D) [Batch, Time, Dim]
        :returns: TODO

        """
        x = torch.nn.utils.rnn.pack_padded_sequence(x,
                                                    lengths,
                                                    batch_first=True,
                                                    enforce_sorted=False)
        x, _ = self.net(x)
        x, _ = torch.nn.utils.rnn.pad_packed_sequence(x, batch_first=True)
        pooled_x = self.pooling(x, lengths)
        pred = self.outputlayer(pooled_x['output'])
        output = {'output': pred}
        if 'weight' in pooled_x:  # in case of attention, just add the weight for later keshihua
            output['weight'] = pooled_x['weight']
        return output


class LSTM(torch.nn.Module):
    """LSTM class for Depression detection"""
    def __init__(self, inputdim: int, output_size: int, **kwargs):
        """

        :inputdim:int: Input dimension
        :output_size:int: Output dimension of LSTMSimpleAttn
        :**kwargs: Other args, passed down to nn.LSTMSimpleAttn


        """
        torch.nn.Module.__init__(self)
        model_kwargs = {
            'hidden_size': kwargs.get('hidden_size', 128),
            'num_layers': kwargs.get('num_layers', 3),
            'dropout': kwargs.get('dropout', 0.2),
            'bidirectional': kwargs.get('bidirectional', True)
        }
        self.net = nn.LSTM(input_size=inputdim, **model_kwargs)
        init_rnn(self.net)
        pooling_kwargs = {
            'pooling': kwargs.get('pooling', 'attention'),
            'outputdim': kwargs.get(
                'outputdim',
                1),  # In case of attention: 2 or 1 or how many attentions
        }
        self.pooling = parse_poolingfunction(
            pooling_kwargs['pooling'],
            inputdim=model_kwargs['hidden_size'] * 2,
            outputdim=pooling_kwargs['outputdim'])
        linear_dim = model_kwargs['hidden_size'] * 2
        if pooling_kwargs['pooling'] == 'stat':
            linear_dim *= 3
        self.outputlayer = nn.Sequential(nn.Linear(linear_dim, output_size))

    def forward(self, x, lengths):
        """Forwards input vector through network

        :x: input tensor of shape (B, T, D) [Batch, Time, Dim]
        :returns: TODO

        """
        x = torch.nn.utils.rnn.pack_padded_sequence(x,
                                                    lengths,
                                                    batch_first=True,
                                                    enforce_sorted=False)
        x, _ = self.net(x)
        x, _ = torch.nn.utils.rnn.pad_packed_sequence(x, batch_first=True)
        pooled_x = self.pooling(x, lengths)
        pred = self.outputlayer(pooled_x['output'])
        output = {'output': pred}
        if 'weight' in pooled_x:  # in case of attention, just add the weight for later keshihua
            output['weight'] = pooled_x['weight']
        return output


class HierGRU(torch.nn.Module):
    """GRU with Hierarchical pooling class for Depression detection"""
    def __init__(self, inputdim: int, output_size: int, **kwargs):
        """

        :inputdim:int: Input dimension
        :output_size:int: Output dimension of GRUAttn
        :**kwargs: Other args, passed down to nn.GRUAttn


        """
        torch.nn.Module.__init__(self)
        num_layers = kwargs.get('num_layers', 3)
        self.model_kwargs = {
            'hidden_size': kwargs.get('hidden_size', 128),
            'bidirectional': kwargs.get('bidirectional', True)
        }
        self.net = nn.ModuleList([
            nn.GRU(input_size=inputdim, **self.model_kwargs)
            for _ in range(num_layers)
        ])
        init_rnn(self.net)
        pooling_kwargs = {
            'pooling': kwargs.get('pooling', 'max'),
            'outputdim': kwargs.get(
                'outputdim',
                1),  # In case of attention: 2 or 1 or how many attentions
        }
        self.pooling = parse_poolingfunction(
            pooling_kwargs['pooling'],
            inputdim=self.model_kwargs['hidden_size'] * 2,
            outputdim=pooling_kwargs['outputdim'])
        self.outputlayer = nn.Linear(self.model_kwargs['hidden_size'] * 6,
                                     output_size)

    def forward(self, x, lengths):
        """Forwards input vector through network

        :x: input tensor of shape (B, T, D) [Batch, Time, Dim]
        :returns: TODO

        """
        batch_size, length, dim = x.shape
        x = torch.nn.utils.rnn.pack_padded_sequence(x,
                                                    lengths,
                                                    batch_first=True,
                                                    enforce_sorted=False)
        outs, weights = [], []
        h = torch.zeros(2, batch_size, self.model_kwargs['hidden_size'])

        for stage_model in self.net:
            out, h = stage_model(x, h)
            out, _ = torch.nn.utils.rnn.pad_packed_sequence(out,
                                                            batch_first=True)
            pooled_x = self.pooling(out, lengths)
            outs.append(pooled_x['output'])
            if 'weight' in pooled_x:
                weights.append(pooled_x['weight'])
        out = torch.cat(outs, -1)
        pred = self.outputlayer(out)
        output = {'output': pred}
        if len(weights) > 0:
            weights = torch.cat(weights, -1)
            output['weight'] = weights
        return output


if __name__ == "__main__":
    m = GRU(300, 2, pooling='mm')
    x = torch.randn(3, 100, 300)
    lengths = torch.tensor([30, 10, 50])
    print(x)
    y = m(x, lengths)
    print(y)
