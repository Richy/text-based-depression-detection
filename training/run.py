# coding=utf-8
#!/usr/bin/env python3
import datetime
import torch
from pprint import pformat
import models
from pathlib import Path
from dataset import create_dataloader, MinimumOccupancySampler
import fire
import losses
import pandas as pd
import utils
import numpy as np
from sklearn import metrics
from sklearn.model_selection import StratifiedKFold
import uuid
from tabulate import tabulate
import os
from ignite.contrib.handlers import ProgressBar
from ignite.engine import (Engine, Events)
from ignite.handlers import EarlyStopping, ModelCheckpoint
from ignite.metrics import Loss, RunningAverage, ConfusionMatrix, MeanAbsoluteError, Precision, Recall, Accuracy
from ignite.contrib.handlers.param_scheduler import LRScheduler
import warnings
warnings.filterwarnings('ignore')

device = 'cpu'
if torch.cuda.is_available(
) and 'SLURM_JOB_PARTITION' in os.environ and 'gpu' in os.environ[
        'SLURM_JOB_PARTITION']:
    device = 'cuda'
    # Without results are slightly inconsistent
    torch.backends.cudnn.deterministic = True
DEVICE = torch.device(device)


class Runner(object):
    """docstring for Runner"""
    def __init__(self, seed=0):
        super(Runner, self).__init__()
        self.seed = seed
        torch.manual_seed(seed)
        np.random.seed(seed)
        if device == 'cuda':
            torch.cuda.manual_seed(seed)

    @staticmethod
    def _forward(model, batch):
        inputs, targets, fname, lengths = batch
        inputs, targets = inputs.float().to(DEVICE), targets.float().to(DEVICE)
        output = model(inputs, lengths)['output']
        return output, targets

    def train(self, config, **kwargs):
        config_parameters = utils.parse_config_or_kwargs(config, **kwargs)
        if 'outputpath' in config_parameters:
            outputdir = Path(config_parameters['outputpath'])
        elif 'basepath' in config_parameters:
            outputdir = Path(
                config_parameters['basepath'], config_parameters['model'],
                "{}_{}".format(
                    datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%m'),
                    uuid.uuid1().hex[:6]))

        checkpoint_handler = ModelCheckpoint(
            outputdir,
            'run',
            n_saved=1,
            require_empty=False,
            create_dir=True,
            score_function=lambda engine: -engine.state.metrics['Loss'],
            score_name='loss')
        torch.save(config_parameters, Path(outputdir) / 'run_config.pth')

        logger = utils.genlogger(Path(outputdir) / 'train.log')
        logger.info("Experiment is stored in {}".format(outputdir))
        logger.info("Seed is {}".format(self.seed))
        for line in pformat(config_parameters).split('\n'):
            logger.info(line)

        # DataRelated
        if isinstance(
                config_parameters['trainlabels'], pd.DataFrame
        ):  # In case if other function calls this train function with passed label_df (e.g., stratified)
            train_df = config_parameters['trainlabels']
        if 'cvlabels' in config_parameters and isinstance(
                config_parameters['cvlabels'], pd.DataFrame):
            cv_df = config_parameters['cvlabels']
        elif isinstance(
                config_parameters['trainlabels'],
                str):  # Read from file and generate train/cv split manually
            logger.info(
                "Splitting manually Train/cv from trainlabels [{}]".format(
                    config_parameters['trainlabels']))
            label_df = pd.read_csv(
                config_parameters['trainlabels']).convert_dtypes()
            train_df, cv_df = utils.split_train_cv(label_df)
            logger.info("Trainsize: {} Cvsize: {}".format(
                train_df.shape, cv_df.shape))

        sampler = MinimumOccupancySampler(np.stack(train_df['PHQ8_Binary']))
        logger.info("Using MinimumOccupancySampler")

        data_hdf = config_parameters['traindata']
        train_dataloader = create_dataloader(
            data_hdf,
            train_df,
            shuffle=False,  # Just because sampler is already shuffling
            sampler=sampler,
            **config_parameters['dataloader_args'])
        cv_dataloader = create_dataloader(
            data_hdf,
            cv_df,
            shuffle=False,
            **config_parameters['dataloader_args'])
        inputdim = train_dataloader.dataset.datadim
        n_labels = 2
        logger.info("Features: {} Input dimension: {}".format(
            config_parameters['traindata'], inputdim))
        model = getattr(models, config_parameters['model'])(
            inputdim=inputdim,
            output_size=n_labels,
            **config_parameters['model_args'])
        if 'pretrain' in config_parameters:
            logger.info("Loading pretrained model {}".format(
                config_parameters['pretrain']))
            pretrained_model = torch.load(config_parameters['pretrain'],
                                          map_location='cpu')
            model.load_state_dict(pretrained_model)
        logger.info("<== Model ==>")
        for line in pformat(model).split('\n'):
            logger.info(line)
        criterion = getattr(
            losses,
            config_parameters['loss'])(**config_parameters['loss_args'])
        optimizer = getattr(torch.optim, config_parameters['optimizer'])(
            list(model.parameters()) + list(criterion.parameters()),
            **config_parameters['optimizer_args'])
        criterion = criterion.to(device)
        model = model.to(device)

        def _train_batch(_, batch):
            model.train()
            with torch.enable_grad():
                optimizer.zero_grad()
                outputs, targets = self._forward(model, batch)
                loss = criterion(outputs, targets)
                loss.backward()
                torch.nn.utils.clip_grad_norm_(model.parameters(), 0.5)
                optimizer.step()
                return loss.item()

        def _inference(_, batch):
            model.eval()
            with torch.no_grad():
                return self._forward(model, batch)

        def meter_transform(output):
            y_pred, y_true = output
            # y_pred is of shape [Bx2] (0 = MSE, 1 = BCE)
            # y = is of shape [Bx2] (0=Mse, 1 = BCE)
            ret = {
                'y_pred': torch.sigmoid(y_pred[:, 1]).round().long(),
                'y': y_true[:, 1].long()
            }
            return ret

        acc = Accuracy(output_transform=meter_transform)
        metrics = {
            'Loss':
            Loss(criterion),
            "Accuracy":
            acc,
            'MAE':
            MeanAbsoluteError(
                output_transform=lambda out: (out[0][:, 0], out[1][:, 0])),
        }

        train_engine = Engine(_train_batch)
        inference_engine = Engine(_inference)
        for name, metric in metrics.items():
            metric.attach(inference_engine, name)
        pbar = ProgressBar(persist=False)
        pbar.attach(train_engine)

        scheduler = getattr(torch.optim.lr_scheduler,
                            config_parameters['scheduler'])(
                                optimizer,
                                **config_parameters['scheduler_args'])
        early_stop_handler = EarlyStopping(
            patience=10,
            score_function=lambda engine: -engine.state.metrics['Loss'],
            trainer=train_engine)
        inference_engine.add_event_handler(Events.EPOCH_COMPLETED,
                                           early_stop_handler)
        inference_engine.add_event_handler(Events.EPOCH_COMPLETED,
                                           checkpoint_handler, {
                                               'model': model,
                                           })

        @train_engine.on(Events.EPOCH_COMPLETED)
        def compute_metrics(engine):
            inference_engine.run(cv_dataloader)
            validation_string_list = [
                "Validation Results - Epoch: {:<3}".format(engine.state.epoch)
            ]
            for metric in metrics:
                validation_string_list.append("{}: {:<5.2f}".format(
                    metric, inference_engine.state.metrics[metric]))
            logger.info(" ".join(validation_string_list))

        @inference_engine.on(Events.COMPLETED)
        def update_reduce_on_plateau(engine):
            val_loss = engine.state.metrics['Loss']
            if 'ReduceLROnPlateau' == scheduler.__class__.__name__:
                scheduler.step(val_loss)
            else:
                scheduler.step()

        train_engine.run(train_dataloader,
                         max_epochs=config_parameters['epochs'])
        # Final evaluation on the dev set
        logger.info("Performance on CV:")
        model.load_state_dict(torch.load(checkpoint_handler.last_checkpoint))
        cv_pred_scores = self._evaluate(model, data_hdf, cv_df)
        for line in cv_pred_scores.to_markdown().split('\n'):
            logger.info(line)

        # Return for further processing
        return outputdir

    def train_kfold(self, config, nfolds: int = 5, **kwargs):
        config_parameters = utils.parse_config_or_kwargs(config, **kwargs)
        label_df = pd.read_csv(
            config_parameters['trainlabels']).convert_dtypes().reset_index()
        logger = utils.genlogger()
        outputpath = Path(
            config_parameters['basepath'], config_parameters['model'],
            "{}_{}".format(
                datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%m'),
                uuid.uuid1().hex))
        X = range(len(label_df))
        Y = label_df['PHQ8_Binary'].astype(int).values
        for k, (train_index, cv_index) in enumerate(
                StratifiedKFold(n_splits=nfolds, random_state=42).split(X, Y)):
            train_df = label_df[label_df.index.isin(train_index)].copy()
            cv_df = label_df[label_df.index.isin(cv_index)].copy()
            logger.info("Running KFold {}/{}".format(k + 1, nfolds))
            logger.info("Sizes (Train/CV): ({}/{})".format(
                len(train_df), len(cv_df)))
            cur_outputpath = outputpath / f'fold_{k+1}'
            exp_dir = self.train(config,
                                 trainlabels=train_df,
                                 cvlabels=cv_df,
                                 outputpath=cur_outputpath,
                                 **kwargs)
            self.evaluate(exp_dir)

    @staticmethod
    def _forwardwithdata(model,
                         features,
                         df,
                         batch_size=1,
                         num_workers=2,
                         round_binary=True):
        dataloader = create_dataloader(features,
                                       df,
                                       batch_size=batch_size,
                                       num_workers=num_workers,
                                       shuffle=False)
        y_score_true, y_score_pred, y_binary_pred, y_binary_true = [], [], [], []
        model = model.to(device).eval()
        with torch.no_grad():
            for batch in dataloader:
                output, target = Runner._forward(model, batch)
                y_score_pred.append(output[:, 0].cpu().numpy())
                y_score_true.append(target[:, 0].cpu().numpy())
                binary_output = torch.sigmoid(output[:, 1]).cpu()
                if round_binary:
                    binary_output = binary_output.round()
                y_binary_pred.append(binary_output.numpy())
                y_binary_true.append(target[:, 1].cpu().numpy())
        y_score_true = np.concatenate(y_score_true)
        y_score_pred = np.concatenate(y_score_pred)
        y_binary_pred = np.concatenate(y_binary_pred).astype(float)
        y_binary_true = np.concatenate(y_binary_true).astype(float)
        return y_score_pred, y_score_true, y_binary_pred, y_binary_true

    @staticmethod
    def _calculate_metrics(y_score_pred, y_score_true, y_binary_pred,
                           y_binary_true):
        acc = metrics.accuracy_score(y_binary_true, y_binary_pred)
        rmse = np.sqrt(metrics.mean_squared_error(y_score_true, y_score_pred))
        mae = metrics.mean_absolute_error(y_score_true, y_score_pred)
        ret = {}
        for avg in ('macro', 'micro', 'binary'):
            pre = metrics.precision_score(y_binary_true,
                                          y_binary_pred,
                                          average=avg)
            rec = metrics.recall_score(y_binary_true,
                                       y_binary_pred,
                                       average=avg)
            f1_prime = 2 * pre * rec / (pre + rec + 1e-12)
            f1 = metrics.f1_score(y_binary_true, y_binary_pred, average=avg)
            ret[avg] = {
                'F1': f1,
                'F1_Prime': f1_prime,
                'Precision': pre,
                'Recall': rec,
                'Accuracy': acc,
                'MAE': mae,
                'RMSE': rmse
            }
        df = pd.DataFrame(ret).T
        return df

    def _evaluate(self, model, features, df, batch_size=1, num_workers=2):

        y_score_pred, y_score_true, y_binary_pred, y_binary_true = Runner._forwardwithdata(
            model,
            features,
            df,
            batch_size=batch_size,
            num_workers=num_workers)
        return self._calculate_metrics(y_score_pred, y_score_true,
                                       y_binary_pred, y_binary_true)

    def evaluate(
            self,
            experiment_path: str,
            outputfile: str = 'results.csv',
            return_df=False,
            batch_size=32,  # Should have no effect
            **kwargs):
        """Prints out the stats for the given model ( MAE, RMSE, F1, Pre, Rec)


        """
        logger = utils.genlogger()
        logger.info("Getting config .. ")
        logger.info(f"Running with batch size {batch_size}")
        config = torch.load(next(Path(experiment_path).glob('run_config*')),
                            map_location='cpu')
        logger.info("Loading state dict")
        model_state_dict = torch.load(next(
            Path(experiment_path).glob('run_model*')),
                                      map_location='cpu')
        config_parameters = dict(config, **kwargs)
        eval_features = config_parameters['evaldata']
        eval_df = pd.read_csv(config_parameters['evallabels']).convert_dtypes()
        outputfile = Path(experiment_path) / outputfile
        inputdim = next(iter(model_state_dict.values())).shape[-1]
        model = getattr(models, config_parameters['model'])(
            inputdim=inputdim,
            output_size=2,
            **config_parameters['model_args'])
        model.load_state_dict(model_state_dict)
        logger.info(f"{model}")
        df = self._evaluate(model,
                            eval_features,
                            eval_df,
                            batch_size=batch_size)
        df.to_csv(outputfile, float_format="%.3f")

        print(df.to_markdown())
        if return_df:
            return df

    def evaluate_kfold(self,
                       experiment_path: str,
                       outputfile: str = 'results_kfoldfusion.csv',
                       return_df=False,
                       batch_size=32,
                       **kwargs):
        logger = utils.genlogger()
        all_score_pred, all_binary_pred = [], []
        for fold_dir in Path(experiment_path).iterdir():
            if not fold_dir.is_dir():
                continue
            logger.info(f"Evaluating model from {fold_dir}")
            config = torch.load(next(Path(fold_dir).glob('run_config*')),
                                map_location='cpu')
            logger.info("Loading state dict")
            model_state_dict = torch.load(next(
                Path(fold_dir).glob('run_model*')),
                                          map_location='cpu')
            config_parameters = dict(config, **kwargs)
            eval_features = config_parameters['evaldata']
            eval_df = pd.read_csv(
                config_parameters['evallabels']).convert_dtypes()
            inputdim = next(iter(model_state_dict.values())).shape[-1]
            model = getattr(models, config_parameters['model'])(
                inputdim=inputdim,
                output_size=2,
                **config_parameters['model_args'])
            model.load_state_dict(model_state_dict)
            y_score_pred, y_score_true, y_binary_pred, y_binary_true = Runner._forwardwithdata(
                model,
                eval_features,
                eval_df,
                batch_size=batch_size,
                round_binary=False,
            )
            all_score_pred.append(y_score_pred)
            all_binary_pred.append(y_binary_pred)
        all_score_pred = np.mean(all_score_pred, axis=0)
        all_binary_pred = np.mean(all_binary_pred, axis=0).round()
        df = self._calculate_metrics(all_score_pred, y_score_true,
                                     all_binary_pred, y_binary_true)
        outputfile = Path(experiment_path) / outputfile
        df.to_csv(outputfile, float_format="%.3f")

        print(df.to_markdown())
        if return_df:
            return df


if __name__ == '__main__':
    fire.Fire(Runner)
