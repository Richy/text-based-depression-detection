#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from pprint import pformat
from sklearn.model_selection import train_test_split

import numpy as np
import pandas as pd
import torch
import tqdm
import yaml
from loguru import logger


def parse_config_or_kwargs(config_file, **kwargs):
    with open(config_file) as con_read:
        yaml_config = yaml.load(con_read, Loader=yaml.FullLoader)
    # passed kwargs will override yaml config
    # for key in kwargs.keys():
    # assert key in yaml_config, "Parameter {} invalid!".format(key)
    return dict(yaml_config, **kwargs)


def genlogger(outputfile=None):
    log_format = "[<green>{time:YYYY-MM-DD HH:mm:ss}</green>] {message}"
    logger.configure(handlers=[{"sink": sys.stderr, "format": log_format}])
    if outputfile:
        logger.add(outputfile, enqueue=True, format=log_format)
    return logger


def split_train_cv(df, frac=0.85, mode='stratified'):
    if mode == 'stratified':
        idx_train, idx_cv, _, _ = train_test_split(np.arange(len(df)),
                                                   df['PHQ8_Binary'],
                                                   test_size=1 - frac,
                                                   random_state=21)
        train_df = df[df.index.isin(idx_train)].copy()
        cv_df = df[df.index.isin(idx_cv)].copy()
    else:
        train_df = df.sample(frac=frac, random_state=1)
        cv_df = df[~df.index.isin(train_df.index)].copy()
    return train_df, cv_df
